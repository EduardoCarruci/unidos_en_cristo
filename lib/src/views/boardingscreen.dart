import 'package:flutter/material.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';
import 'package:unidos_en_cristo/src/screens/home.dart';
import 'package:unidos_en_cristo/src/utils/constants.dart';

class OnBoardingPage extends StatefulWidget {
  @override
  _OnBoardingPageState createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {
  void _onIntroEnd(context) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => HomeScreen()),
    );
  }

  final pages = [
    PageViewModel(
      pageColor: Colors.white,
      body: Text(
        'El poder lo tienes en tus manos',
      ),
      title: Text(
        'Unidos en Cristo',
      ),
      titleTextStyle: Constants.styleTextTitle,
      bodyTextStyle: Constants.bodyTextTitle,
      mainImage: imagePage("assets/images/intro1.png"),
    ),
    PageViewModel(
      pageColor: Colors.white,
      body: Text(
        'Agua, fuente de vida',
      ),
      title: Text('Transforma vidas'),
      mainImage: imagePage("assets/images/intro2.png"),
      titleTextStyle: Constants.styleTextTitle,
      bodyTextStyle: Constants.bodyTextTitle,
    ),
    PageViewModel(
      pageColor: Colors.white,
      body: Text(
        'Empieza ahora a cuidar el agua',
      ),
      title: Text('Es tu momento'),
      mainImage: imagePage("assets/images/intro3.png"),
      titleTextStyle: Constants.styleTextTitle,
      bodyTextStyle: Constants.bodyTextTitle,
    ),
  ];

  static Widget imagePage(String image) {
    return Image.asset(
      image,
      height: 285.0,
      width: 285.0,
      alignment: Alignment.center,
    );
  }

  Widget textcustomize(String text) {
    return Text(
      text,
      style: TextStyle(color: Colors.grey),
    );
  }

  @override
  Widget build(BuildContext context) {
    return IntroViewsFlutter(
      pages,
      showNextButton: true,
      showBackButton: true,
      skipText: textcustomize("SKIP"),
      backText: textcustomize("BACK"),
      nextText: textcustomize("NEXT"),
      doneText: textcustomize("DONE"),
      onTapSkipButton: () {
        _onIntroEnd(context);
      },
      onTapDoneButton: () {
        _onIntroEnd(context);
      },
      pageButtonTextStyles: TextStyle(
        color: Colors.white,
        fontSize: 18.0,
      ),
    );
  }
}


