import 'package:flutter/material.dart';


class Start extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return(Container(
      height: 250,
      width: 350,
      margin: EdgeInsets.only(top: 170.0,left: 27.0),
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.scaleDown,
          image: AssetImage("assets/images/logo.png"),

        ),
      ),
    )
    );
  }

}