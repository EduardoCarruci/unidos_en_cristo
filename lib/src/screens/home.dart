import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'file:///C:/Users/MIguel/Desktop/unidos_en_cristo/lib/src/widgets/Study_Actual/study_actual.dart';
import 'package:unidos_en_cristo/src/screens/versiculos.dart';
import 'package:unidos_en_cristo/src/screens/video.dart';
import 'package:unidos_en_cristo/src/utils/providers.dart';
import 'package:unidos_en_cristo/src/utils/styles.dart';

import 'news.dart';
import 'study.dart';
import 'start.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  GlobalKey _bottomNavigationKey = GlobalKey();
  int pageIndex = 0;
  Widget _show = new Start();
  final SafeHome _safeHome = SafeHome();
  final Study _study = Study();
  final Start _start = Start();
  final Video _video = Video();
  final Versiculos _versiculos = Versiculos();
  final MoviesConceptPage moviesConceptPage = MoviesConceptPage();
  @override
  Widget build(BuildContext context) {
    final providerInfo = Provider.of<ProviderInfo>(context);
    return Scaffold(
        //appBar: AppBar(title: Text('Unidos en Cristo')),
        bottomNavigationBar: CurvedNavigationBar(
          key: _bottomNavigationKey,
          index: providerInfo.index,
          height: 50.0,
          items: <Widget>[
            Icon(Icons.home, size: 30, color: Colors.white),
            Icon(Icons.account_balance, size: 30, color: Colors.white),
            Icon(Icons.library_books, size: 30, color: Colors.white),
            Icon(Icons.people, size: 30, color: Colors.white),
            Icon(Icons.star, size: 30, color: Colors.white),
          ],
          color: Styles.navegationBarCurved,
          buttonBackgroundColor: Styles.navegationBarCurved,
          backgroundColor: Colors.white10,
          animationCurve: Curves.easeInOut,
          animationDuration: Duration(milliseconds: 450),
          onTap: (int tappedIndex) {
            providerInfo.index = tappedIndex;
            _show = chooser(providerInfo.index);
            setState(() {});
          },
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _show // NO PUEDES BORRAR ESTA LINEA
            ],
          ),
        ));
  }

  Widget chooser(int page) {
    switch (page) {
      case 0:
        return _start;
        break;
      case 1:
        return moviesConceptPage;
        break;

      case 2:
        return _safeHome;
        break;
      case 3:
        return _video;
        break;
      case 4:
     
        return _versiculos;
        break;
      default:
      
        break;
    }
  }
}
