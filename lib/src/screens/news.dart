import 'package:flutter/material.dart';
import 'package:unidos_en_cristo/src/widgets/News/gradient_back.dart';
import 'package:unidos_en_cristo/src/widgets/News/title_text_news.dart';
import 'package:unidos_en_cristo/src/widgets/News/card_image_news.dart';
import 'package:unidos_en_cristo/src/widgets/News/categories_news.dart';


class RowCardShort extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    double width_card_short=150;

    final row_cards_short=Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        CardImage(width_card_short),
        CardImage(width_card_short)],
    );

    return row_cards_short;
  }

}

class SafeHome extends StatefulWidget {

  @override
  _SafeHomeState createState() => _SafeHomeState();
}

//pull
class _SafeHomeState extends State<SafeHome> {


  double width_card_long=310;


  @override
  Widget build(BuildContext context) {

    return Stack(
        children: <Widget>[
          GradientBack(),
         SingleChildScrollView(
          child:
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Title_text_news(),
                Container(
                  height: 80,
                  width: double.infinity,
                  //color: Colors.red,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(

                      children: <Widget>[
                        CategoriesNews("assets/images/food.png", "Politica"),
                        CategoriesNews("assets/images/food2.png", "Food"),
                        CategoriesNews("assets/images/food3.png", "Vida"),
                        CategoriesNews("assets/images/food4.png", "Ciencia"),
                        CategoriesNews("assets/images/food5.png", "Salud"),
                        CategoriesNews("assets/images/heal.png", "Tecnologia"),
                        CategoriesNews("assets/images/food.png", "Food"),
                      ],
                    ),
                  ),
                ),


            Container(
              padding: EdgeInsets.only(right: 40.0, left: 40.0,top: 10.0),
              margin: EdgeInsets.only(top:15.0),
              //height: MediaQuery.of(context).size.height * .627,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Color(0xFF13242f),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(60),
                      topRight: Radius.circular(60))),



              child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      RowCardShort(),
                      CardImage(width_card_long),
                      RowCardShort(),
                      CardImage(width_card_long),
                    ],
                  ),

              ),
            )
          ]),

        ),
        ],
    );
  }



}
