import 'package:flutter/material.dart';
import 'package:unidos_en_cristo/src/animated/fadeanimation.dart';
import 'package:unidos_en_cristo/src/widgets/News/gradient_back.dart';
import 'package:unidos_en_cristo/src/widgets/Verse/card_image_verse.dart';

class RowCardShort extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width_card_short = 150;

    final row_cards_short = Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        CardImage(width_card_short),
        CardImage(width_card_short)
      ],
    );

    return row_cards_short;
  }
}

class Versiculos extends StatefulWidget {
  @override
  State<Versiculos> createState() {
    return _VersiculosState();
  }
}

class _VersiculosState extends State<Versiculos> {
  double width_card_long = 310;

  List <Widget>arr = [RowCardShort(),RowCardShort(),RowCardShort(),RowCardShort()];


  final title = SafeArea(
    child: Column(
    children: <Widget>[
      SizedBox(
        height: 10,
      ),
      FadeAnimation(
        0.3,
        Text(
          "Versiculos",
          style: TextStyle(color: Colors.white, fontSize: 40),
        ),
      )
    ],
    ),
  );


  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        GradientBack(),
        SingleChildScrollView(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                title,
                Container(
                  padding: EdgeInsets.only(right: 40.0, left: 40.0, top: 10.0),
                  margin: EdgeInsets.only(top: 15.0),
                  //height: MediaQuery.of(context).size.height * .627,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Color(0xFF13242f),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(60),
                          topRight: Radius.circular(60))),

                  child: SingleChildScrollView(
                    child: Column(
                      children: arr
                    ),
                  ),
                )
              ]),
        ),
      ],
    );
  }
}
