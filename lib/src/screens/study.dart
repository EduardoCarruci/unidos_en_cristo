import 'package:flutter/material.dart';
import 'package:unidos_en_cristo/src/screens/start.dart';

import 'package:unidos_en_cristo/src/utils/styles.dart';
import 'package:unidos_en_cristo/src/widgets/Study/cardimage.dart';

class Study extends StatefulWidget {
  @override
  State<Study> createState() {
    return StateStudy();
  }
}

class StateStudy extends State<Study> {
  List colorsCards1 = [Colors.lightGreen, Styles.black, Colors.lightGreen];
  List colorsCards2 = [
    Colors.lightBlueAccent,
    Styles.black,
    Colors.lightBlueAccent
  ];
  List colorsCards3 = [Colors.redAccent, Styles.black, Colors.redAccent];
  String imageText1 = "assets/images/textVersiculo.png";
  String imageText2 = "assets/images/textTemas.png";
  String imageText3 = "assets/images/textDevocional.png";
  String imageCard1 = "assets/images/versiculos.png";
  String imageCard2 = "assets/images/temas.png";
  String imageCard3 = "assets/images/devocional.png";

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(children: <Widget>[
        CardImage(colorsCards2, imageText1, imageCard1),
        CardImage(colorsCards1, imageText2, imageCard2),
        CardImage(colorsCards3, imageText3, imageCard3),
      ]),
    );
  }
}
