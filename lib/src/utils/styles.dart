import 'dart:ui';
import 'package:flutter/material.dart';

// primaryColor = Color.fromRGBO(0, 255, 255, 1);

class Styles {



  static Color appDrawerIconColor = Colors.grey[800];
  static Color gold = Color.fromRGBO(158, 131, 15, 1);
  static Color blue = Color(0xFF15a7f9);
  static Color circleIcon =Color(0xFF1385f8);
  static Color black = Colors.grey[900];
  static Color navegationBarCurved = Colors.grey[900];
  static TextStyle appDrawerTextStyle = TextStyle(color: Colors.grey[900]);



}
