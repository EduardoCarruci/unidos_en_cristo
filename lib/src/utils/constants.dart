import 'package:flutter/material.dart';

class Constants {
  static TextStyle styleTextTitle =
      new TextStyle(fontFamily: 'Comfortaa', color: Colors.grey[700]);


  static TextStyle bodyTextTitle =    TextStyle(
        color: Colors.grey[700],
        fontSize: 25.0,
        fontStyle: FontStyle.italic,
      );

    
}
