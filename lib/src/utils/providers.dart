import 'package:flutter/material.dart';

class ProviderInfo with ChangeNotifier {
  int _index = 0;

  get index {
    return _index;
  }

  set index(int pos) {
    this._index = pos;
    notifyListeners();
  }
}
