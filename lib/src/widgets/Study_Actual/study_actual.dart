import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:unidos_en_cristo/src/utils/styles.dart';
import 'package:unidos_en_cristo/src/screens/versiculos.dart';
import 'package:unidos_en_cristo/src/utils/providers.dart';

class Movie {
  final String path;
  final String title;

  const Movie({this.path, this.title});
}

const movies = [
  const Movie(
      path:
      "assets/images/biblia.jpg",
      title: "assets/images/textVersiculos.jpg"),
  const Movie(
      path:
      "assets/images/temas.jpg",
      title: "assets/images/textTemas.png"),
  const Movie(
      path: "assets/images/agradecida.jpg",
      title: "assets/images/textDevocional.png"),
];

class MoviesConceptPage extends StatefulWidget {
  @override
  _MoviesConceptPageState createState() => _MoviesConceptPageState();
}

class _MoviesConceptPageState extends State<MoviesConceptPage> {
  final pageController = PageController(viewportFraction: 0.7);
  final ValueNotifier<double> _pageNotifier = ValueNotifier(0.0);
  Widget _show = new Versiculos();
 
  int bandera = 1;

  void _listener() {
    _pageNotifier.value = pageController.page;
    setState(() {});
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      pageController.addListener(_listener);
    });
    super.initState();
  }

  @override
  void dispose() {
    pageController.removeListener(_listener);
    pageController.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    final borderRadius = BorderRadius.circular(30);
    final size = MediaQuery.of(context).size;
    final providerInfo = Provider.of<ProviderInfo>(context);
    return bandera == 1
        ? moviesConcepts(borderRadius, size, providerInfo)
        : _show;
    
  } //data

  Widget moviesConcepts(borderRadius, size, providerInfo) {
    return Stack(
      children: [
        Positioned.fill(bottom: 100,
          child: ValueListenableBuilder<double>(
              valueListenable: _pageNotifier,
              builder: (context, value, child) {
                return Stack(
                  children: movies.reversed
                      .toList()
                      .asMap()
                      .entries
                      .map(
                        (entry) => Positioned.fill(bottom: 100,
                          child: ClipRect(
                            clipper: MyClipper(
                              percentage: value,
                              title: entry.value.title,
                              index: entry.key,
                            ),
                            child:  Image(
                              image: AssetImage(entry.value.path,
                              ),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      )
                      .toList(),
                );
              }),
        ),
        Positioned(
          left: 0,
          right: 0,
          bottom: 10,
          height: size.height / 4,
          child: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [
                Colors.white,
                Colors.white,
                Colors.white,
                Colors.white60,
                Colors.white24,
              ],
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
            )),
          ),
        ),




        Container(
          height: MediaQuery.of(context).size.height/1.1,
          width: double.infinity,
          child: PageView.builder(
              itemCount: movies.length,
              controller: pageController,
              itemBuilder: (context, index) {
                final lerp =
                    lerpDouble(0, 1, (index - _pageNotifier.value).abs());

                double opacity =
                    lerpDouble(0.0, 0.5, (index - _pageNotifier.value).abs());
                if (opacity > 1.0) opacity = 1.0;
                if (opacity < 0.0) opacity = 0.0;
                return Transform.translate(
                  offset: Offset(0.0, lerp * 50),
                  child: Opacity(
                    opacity: (1 - opacity),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Card(
                        color: Colors.white,
                        borderOnForeground: true,
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                          borderRadius: borderRadius,
                        ),
                        clipBehavior: Clip.hardEdge,
                        child: SizedBox(
                          height: size.height / 1.7,
                          width: size.width,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [

                              //////////////////////
                              /////Imagen fondo/////
                              ///////////////////////

                              Expanded(
                                flex: 2,
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      top: 20.0, left: 23.0, right: 23.0),
                                  child: ClipRRect(
                                    borderRadius: borderRadius,
                                    child: Image(
                                      image: AssetImage(
                                          movies[index].path,
                                      ),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),


                              //////////////////////
                              /////Titulo imagen/////
                              ///////////////////////

                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      top: 20.0,bottom: 10.0),

                                    child: Image(
                                      image: AssetImage(
                                        movies[index].title,

                                      ),
                                      fit: BoxFit.cover,
                                    ),
                                ),
                              ),


                              //////////////////////
                              /////Boton Entrar/////
                              ///////////////////////


                              Expanded(
                                child: Column(
                                children: [
                                Positioned(
                                  left: MediaQuery.of(context).size.width / 4,
                                  bottom: 120,
                                  width: MediaQuery.of(context).size.width / 2,
                                  child: RaisedButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(10.0)
                                      ),
                                      color: Styles.navegationBarCurved,
                                      child: Text(
                                        'Entra',
                                        style: TextStyle(color: Colors.white),

                                      ),
                                      onPressed: () {
                                        bandera = 0;

                                        setState(() {});
                                      }),
                                ),
                              ], ), ),

                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              }),
        ),

        //////////////////////
        /////Boton ticket/////
       ///////////////////////

      ],
    );
  }
}



class MyClipper extends CustomClipper<Rect> {
  final double percentage;
  final String title;
  final int index;

  MyClipper({
    this.percentage = 0.0,
    this.title,
    this.index,
  });

  @override
  Rect getClip(Size size) {
    int currentIndex = movies.length - 1 - index;
    final realPercent = (currentIndex - percentage).abs();
    if (currentIndex == percentage.truncate()) {
      return Rect.fromLTWH(
          0.0, 0.0, size.width * (1 - realPercent), size.height);
    }
    if (percentage.truncate() > currentIndex) {
      return Rect.fromLTWH(0.0, 0.0, 0.0, size.height);
    }
    return Rect.fromLTWH(0.0, 0.0, size.width, size.height);
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) => true;
}
