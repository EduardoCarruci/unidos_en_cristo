import 'package:flutter/material.dart';
import 'package:unidos_en_cristo/src/animated/fadeanimation.dart';

class CardImage extends StatelessWidget{

   double width_card=150;
   CardImage(this.width_card);

  @override
  Widget build(BuildContext context) {

    final cardImage=
    FadeAnimation(
        1.4,
        Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                ClipRRect(
                  borderRadius:
                  BorderRadius.circular(10.0),
                  child: Container(
                    width: width_card,
                    height: 200,
                    child: Image(
                      image: AssetImage(
                        "assets/images/donut.jpg",
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Positioned(
                  left: 10.0,
                  top: 10.0,
                  child: ClipRRect(
                    borderRadius:
                    BorderRadius.circular(25.0),
                    child: Container(
                      padding: EdgeInsets.all(5),
                      color: Colors.yellow,
                      child: Text(
                        "Comida",
                        style: TextStyle(
                            fontWeight:
                            FontWeight.bold),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  /*  left: 10.0,
                                            top: 10.0, */
                  left: 10.0,
                  bottom: 50,
                  child: Container(
                    width: 150,
                    /*  height: 50,
                                              width: 150, */

                    //color: Colors.red,
                    child: Text(
                      "Tasty Donuts In 30 mins",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0,
                  child: Container(
                    height: 50.0,
                    width: 150,
                    //color: Colors.green,
                    child: Row(
                      crossAxisAlignment:
                      CrossAxisAlignment.center,
                      mainAxisAlignment:
                      MainAxisAlignment
                          .spaceAround,
                      children: <Widget>[
                        Text("2"),
                        Text("5"),
                        Text("Just now")
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ));


    return Padding(padding: const EdgeInsets.only(right:10.0,top:15.0),
        child: cardImage
    );
  }

}