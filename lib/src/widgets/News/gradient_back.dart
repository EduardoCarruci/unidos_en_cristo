import 'package:flutter/material.dart';

class GradientBack extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final gradientback = Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height * 0.925 ,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.topRight,
            colors: [Color(0xFF17c8fa), Color(0xFF15a7f9), Color(0xFF1385f8)]),
      ),
    );

    return gradientback;
  }
}
