import 'package:flutter/material.dart';

class CategoriesNews extends StatelessWidget{

  String texto="";
  String image="";
  CategoriesNews(this.image,this.texto);
  @override
  Widget build(BuildContext context) {

    final categories= Padding(
      padding: const EdgeInsets.only(left: 10.0, right: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ClipOval(
            child: Image(
              image: AssetImage(image),
              height: 40,
              width: 40,
              fit: BoxFit.cover,
            ),
          ),
          Text(texto),
        ],
      ),
    );

    return categories;
  }

}