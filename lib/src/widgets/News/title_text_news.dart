import 'package:flutter/material.dart';
import 'package:unidos_en_cristo/src/animated/fadeanimation.dart';

class Title_text_news extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    final titleText = Column(
      children: <Widget>[
        SizedBox(
          height: 20,
        ),
        FadeAnimation(
            0.3,

            Text(
              "Descubre",
              style: TextStyle(color: Colors.white, fontSize: 40),
            )
        ),
        SizedBox(
          height: 10,
        ),
        FadeAnimation(
          0.5,
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              MaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25.0)),
                onPressed: () {},
                child: Text("Noticias"),
                height: 25,
                color: Colors.white,
              ),
              SizedBox(
                width: 20.0,
              ),
              MaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25.0)),
                onPressed: () {},
                child: Text("Eventos"),
                height: 25,
                color: Colors.lightBlueAccent,
              ),
            ],
          ),
        ),
      ],
    );

    return titleText ;
  }

}