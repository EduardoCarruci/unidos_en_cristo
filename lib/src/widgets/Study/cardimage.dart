import 'package:flutter/material.dart';
import 'package:unidos_en_cristo/src/utils/styles.dart';
import 'package:unidos_en_cristo/src/widgets/Study/bottom_study.dart';

class CardImage extends StatelessWidget {
  List colorsGradient = [Styles.circleIcon, Styles.gold, Styles.circleIcon];
  String pathImageText = "assets/images/textVersiculo.png";
  String pathImage = "assets/images/temas.png";

  CardImage(this.colorsGradient, this.pathImageText, this.pathImage);

  @override
  Widget build(BuildContext context) {
    final card = Align(
      alignment: Alignment.center,
      child: Container(
        height: MediaQuery.of(context).size.height / 2,
        width: 300,
        margin: EdgeInsets.only(
          top: 250.0,right:10.0 ,left:10.0
        ),
        decoration: BoxDecoration(
          gradient: SweepGradient(
            center: Alignment.bottomCenter,
            startAngle: 0.0,
            colors: [
              colorsGradient[0],
              colorsGradient[1],
              colorsGradient[2],
            ],
          ),
          borderRadius: BorderRadius.all(Radius.circular(30.0)),
        ),
      ),
    );

    final imageText = Align(
      alignment: Alignment.center,
      child: Container(
        height: 150,
        width: 250,
        margin: EdgeInsets.only(
          top: 310.0,left:35.0
        ),
        decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.scaleDown, image: AssetImage(pathImageText)),
        ),
      ),
    );

    final image = Align(
      alignment: Alignment.center,
      child: Container(
        height: 220,
        width: 280,
        margin: EdgeInsets.only(top: 115.0 ,left:25.0),
        decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.scaleDown, image: AssetImage(pathImage)),
        ),
      ),
    );



    return Stack(
      children: <Widget>[card, imageText, image,Bottom_study()],

    );
  }
}
