import 'package:flutter/material.dart';
import 'package:unidos_en_cristo/src/screens/versiculos.dart';
import 'package:unidos_en_cristo/src/widgets/News/card_image_news.dart';

class RowCardShort extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width_card_short = 150;

    final row_cards_short = Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        CardImage(width_card_short),
        CardImage(width_card_short)
      ],
    );

    return row_cards_short;
  }
}

class Bottom_study extends StatefulWidget{
  @override
  State<Bottom_study> createState() {
    return _Bottom_study_state();
  }
}

class _Bottom_study_state extends State<Bottom_study>{
  final secondRoute= Versiculos();
  List <Widget>arr = [RowCardShort(),RowCardShort(),RowCardShort()];

  @override
  Widget build(BuildContext context) {
    final bottom = Align(
      alignment: Alignment.center,
      child: Container(
        margin: EdgeInsets.only(top: 450.0,left:105.0),
        child: RaisedButton(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10)),
          elevation: 5.0,
          color: Colors.white,
          onPressed: () {

            setState(() {arr.add(RowCardShort());});
           /* Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => secondRoute),);
            setState(() {});*/
          },
          child: Text('Empieza', style: TextStyle(fontSize: 20)),
        ),
      ),
    );

    return bottom;
  }

}