import 'package:flutter/material.dart';
import 'package:unidos_en_cristo/src/animated/fadeanimation.dart';

class CardImage extends StatelessWidget{

   double width_card=150;
   CardImage(this.width_card);

  @override
  Widget build(BuildContext context) {

    final cardImage=
    FadeAnimation(
        1.4,
        Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                ClipRRect(
                  borderRadius:
                  BorderRadius.circular(10.0),
                  child: Container(
                    width: width_card,
                    height: 150,
                    child: Image(
                      image: AssetImage(
                        "assets/images/donut.jpg",
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),

                Positioned(
                  /*  left: 10.0,
                                            top: 10.0, */
                  left: 10.0,
                  top: 5.0,
                  child: Container(
                    width: 150,
                    /*  height: 50,
                                              width: 150, */

                    //color: Colors.red,
                    child: Text(
                      "Vida",
                      style: TextStyle(
                          color: Colors.black87,
                          fontSize: 30),
                    ),
                  ),
                ),

              ],
            ),
          ],
        ));


    return Padding(padding: const EdgeInsets.only(right:10.0,top:15.0),
        child: cardImage
    );
  }

}