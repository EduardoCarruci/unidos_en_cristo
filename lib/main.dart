import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'src/utils/providers.dart';
import 'src/views/boardingscreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => ProviderInfo(),
        ),
      ],
      child: MaterialApp(
          title: 'Unidos en Cristo',
          debugShowCheckedModeBanner: false,
          initialRoute: 'Splash',
          routes: {
            'Splash': (context) => OnBoardingPage(),
          }),
    );
  }
}
